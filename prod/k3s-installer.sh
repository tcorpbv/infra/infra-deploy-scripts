#!/bin/bash 
set -e
VERSION="v0.2.2"
AAD_SECRET=$1
ARGOCD_SU_PASSWORD=$2
GITLAB_TOKEN=$3

# Embeds sh script so that the error is not caught by set -e unless the final try fails
function retry()
{
        local n=0
        local try=$1
        local cmd="${@: 2}"
        until [[ $n -ge $try ]]
        do
                ((n+=1))
                cat <<EOF | sh && return
                $cmd && break && exit 0 || {
                        if [ $n -lt $try ]; then
                                echo "--- Try $n failed, retrying... ---"
                        else
                                echo "--- ERROR: Try $n failed, exitting... ---"
                        fi
                        sleep 1
                        exit 1
                       }
EOF
        done
        exit 1
}
# The script
echo "TCorp k3s installer $VERSION is starting now..."
mkdir -p /home/ubuntu

echo "Setting up ufw firewall..."
ufw allow 22
ufw allow 6443
ufw enable

echo "Installing k3s (with verified checksum) ..."
echo 20e6a9b6c16ae11a4417d1f7af0002aa31b79159d5ec771da79a2790309d3e49 - > k3ssum.txt
curl -sfL https://raw.githubusercontent.com/rancher/k3s/release-1.17/install.sh | tee install.sh | sha256sum -c k3ssum.txt && INSTALL_K3S_EXEC="server --no-deploy traefik" sh install.sh

cp /etc/rancher/k3s/k3s.yaml /home/ubuntu/.kubeconfig && chown ubuntu /home/ubuntu/.kubeconfig
export KUBECONFIG=/home/ubuntu/.kubeconfig

echo "Installing argocd..."
echo '{"apiVersion":"v1", "kind": "Namespace", "metadata": {"name":"argocd"}}' | kubectl apply -f - # this is better then create namespace argocd because it upserts
echo ff53803c9eaa836684d8a48b325596fd3651fd41d0f8ec189f864ca2caa17241  - > argosum.txt
curl -sfL https://raw.githubusercontent.com/argoproj/argo-cd/v1.4.2/manifests/install.yaml | tee install_argo.yaml | sha256sum -c argosum.txt && kubectl apply -n argocd -f install_argo.yaml

echo "Installing argocd cli..."
echo 25016ff4b51aa12b360d70c7d2b91d24bf17202ac534e7e06184e94a575152dd  - > argoclisum.txt
curl -sfL https://github.com/argoproj/argo-cd/releases/download/v1.4.2/argocd-linux-amd64 | tee argocd-linux-amd64 | sha256sum -c argoclisum.txt && chmod +x argocd-linux-amd64 && mv -f argocd-linux-amd64 /usr/local/bin/argocd

echo "Waiting for pods..."
sleep 10
kubectl wait -n argocd --for=condition=Ready pods --all --timeout=24h
sleep 60

echo "Logging into argocd"
sleep 5
PASSWORD="$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)"
ARGO_ADDRESS="$(kubectl get service -n argocd argocd-server --template '{{.spec.clusterIP}}')"
retry 9 argocd --port-forward-namespace argocd login $ARGO_ADDRESS:443 --username admin --password $PASSWORD --insecure

echo "Changing password of argocd superuser..."
retry 3 argocd account update-password --current-password $PASSWORD --new-password $ARGOCD_SU_PASSWORD

echo "Adding gitlab credentials to argocd..."
retry 3 argocd repocreds add https://gitlab.com/tcorp-k8s --username toonsevrin --password $GITLAB_TOKEN

echo "Making argocd self-managed..."
retry 3 argocd repo add https://gitlab.com/tcorp-k8s/infra/argocd.git --type git
retry 3 argocd app create argocd --repo  https://gitlab.com/tcorp-k8s/infra/argocd.git --dest-namespace argocd --dest-server https://kubernetes.default.svc --revision production --sync-policy automated --path . --project default --upsert

echo "Installing sso onto argocd..."
AAD_ENCODED=$(printf "$AAD_SECRET" | base64)
kubectl patch secret -n argocd argocd-secret --patch '{"data": {"azureactivedirectory.clientsecret": "'$AAD_ENCODED'"}}'
echo "Installing istio outside of argocd..."
echo 8c7d33c22cfe9b4ef965dc0718931527d24a7c33657fa173e011f00b367a82cf - > istiosum.txt
curl -sfL https://github.com/istio/istio/releases/download/1.4.5/istio-1.4.5-linux.tar.gz | tee istio-1.4.5-linux.tar.gz  | sha256sum -c istiosum.txt && tar -xf istio-1.4.5-linux.tar.gz && export PATH=$PWD/istio-1.4.5/bin:$PATH
retry 3 istioctl manifest apply \
  --set profile=demo \
  --set values.gateways.istio-ingressgateway.sds.enabled=true \
  --set values.global.k8sIngress.enableHttps=true \
  --set values.global.k8sIngress.gatewayName=ingressgateway \
  --set values.global.mtls.enabled=true \
  --set values.global.controlPlaneSecurityEnabled=true

echo "Installing argo-apps root onto argocd..."
retry 3 argocd repo add https://gitlab.com/tcorp-k8s/infra/argo-apps.git --type git
retry 3 argocd app create apps --repo https://gitlab.com/tcorp-k8s/infra/argo-apps.git --dest-namespace argocd --dest-server https://kubernetes.default.svc --revision production --sync-policy automated --path . --project default --upsert 

echo ""
echo "--- Finished with the TCorp cloud-init! ---"
echo ""